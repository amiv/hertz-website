# 24HERTZ - Website

This repo contains the files for the static HERTZ website. 

ONLY CHANGE THINGS in the `src` folder

## Installation
1. Install node on your computer
2. Download this repo using git or zip download.
3. Run npm install to install the dependencies (parceljs)
4. To develop with hot reload use `npm run start`
5. After finalizing the changes use `git add .` to add your modifications to the commit list and `git commit -m "Commit msg here"` to upload
6. Nothing, GitLab CI pipelines will build and upload the website